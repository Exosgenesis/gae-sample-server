package com.example;

/**
 * Contains the client IDs and scopes for allowed clients consuming the API.
 */
public class Constants {
  public static final String ANDROID_API_KEY = "AIzaSyDgo_cO818jAvrtPqy6OQaRQ4oEonNcuck";
  public static final String SENDER_ID = "402599269742";
  public static final String WEB_CLIENT_ID = "402599269742-8h26t8vn1cghl14c9q4l7a30685p44ah.apps.googleusercontent.com";
  public static final String ANDROID_CLIENT_ID = "replace this with your Android client ID";
  public static final String IOS_CLIENT_ID = "replace this with your iOS client ID";
  public static final String ANDROID_AUDIENCE = WEB_CLIENT_ID;
  public static final String API_EXPLORER_CLIENT_ID = com.google.api.server.spi.Constant.API_EXPLORER_CLIENT_ID;

  public static final String EMAIL_SCOPE = "https://www.googleapis.com/auth/userinfo.email";
}
