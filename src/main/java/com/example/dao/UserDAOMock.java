package com.example.dao;

import com.example.models.User;

import java.util.LinkedList;
import java.util.List;

public class UserDAOMock implements UserDAO {

    private List<User> users = new LinkedList<>();

    public UserDAOMock() {
        users.add(new User(0L, "Joe", "The most wild shooter from James band."));
        users.add(new User(1L, "Jessie", "A very fast horseman with two guns."));
        users.add(new User(2L, "John", "Who this guy?"));
        users.add(new User(3L, "James", "Leader of the most deadliest criminal group."));
    }

    @Override
    public User addUser(User user) {
        user.setId((long) users.size());
        users.add(user);
        return user;
    }

    @Override
    public void removeUser(User user) {
        users.remove(user);
    }

    @Override
    public void updateUser(User user) {
        users.remove(user);
        users.add(user);
    }

    @Override
    public User getUser(long id) {
        if (id >= users.size())
            return null;
        return users.get((int) id);
    }

    @Override
    public List<User> listUsers() {
        return users;
    }
}
