package com.example.dao;

import com.example.models.User;

import java.util.List;


public interface UserDAO {

    public User addUser(User user);

    public void removeUser(User user);

    public void updateUser(User user);

    public User getUser(long id);

    public List<User> listUsers();
}

