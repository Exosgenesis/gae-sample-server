package com.example.dao;

import com.example.models.User;

import java.util.Collection;
import java.util.List;

import javax.jdo.JDOHelper;
import javax.jdo.JDOObjectNotFoundException;
import javax.jdo.PersistenceManager;
import javax.jdo.PersistenceManagerFactory;

public class UserDAOJdo implements UserDAO {

    private static final PersistenceManagerFactory pmfInstance = JDOHelper
            .getPersistenceManagerFactory("transactions-optional");

    public static PersistenceManagerFactory getPersistenceManagerFactory() {
        return pmfInstance;
    }

    @Override
    public User getUser(long id) {
        try {
            return getPm().getObjectById(User.class, id);
        } catch (JDOObjectNotFoundException ignored) {
            return null;
        }
    }

    public User addUser(User user) {
        PersistenceManager pm = getPm();
        try {
            return pm.makePersistent(user);
        } finally {
            pm.close();
        }
    }

    @SuppressWarnings("unchecked")
    public List<User> listUsers() {
        String query = "select from " + User.class.getName();
        return cutDesc((List<User>) getPm().newQuery(query).execute());
    }

    private List<User> cutDesc(List<User> users) {
        for (User u : users)
            u.setDesc("");
        return users;
    }

    public void removeUser(User user) {
        PersistenceManager pm = getPm();
        try {
            pm.currentTransaction().begin();
            user = pm.getObjectById(User.class, user.getId());
            pm.deletePersistent(user);

            pm.currentTransaction().commit();
        } catch (Exception ex) {
            pm.currentTransaction().rollback();
            throw new RuntimeException(ex);
        } finally {
            pm.close();
        }
    }

    public void updateUser(User user) {
        PersistenceManager pm = getPm();
        String name = user.getName();
        String desc = user.getDesc();

        try {
            pm.currentTransaction().begin();
            user = pm.getObjectById(User.class, user.getId());
            user.setName(name);
            user.setDesc(desc);
            pm.makePersistent(user);
            pm.currentTransaction().commit();
        } catch (Exception ex) {
            pm.currentTransaction().rollback();
            throw new RuntimeException(ex);
        } finally {
            pm.close();
        }
    }

    private PersistenceManager getPm() {
        return getPersistenceManagerFactory().getPersistenceManager();
    }

}