package com.example.exceptions;

import com.google.api.server.spi.ServiceException;


public class GcmAlreadySengingException extends ServiceException {
    private static final int CODE = 400;
    private static final String MSG = "GcmSender already send messages on this device";

    public GcmAlreadySengingException() {
        super(CODE, MSG);
    }

    public GcmAlreadySengingException(Throwable cause) {
        super(CODE, MSG, cause);
    }
}
