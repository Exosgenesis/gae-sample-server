package com.example.services;

import com.example.Constants;
import com.example.dao.UserDAO;
import com.example.dao.UserDAOJdo;
import com.example.exceptions.GcmAlreadySengingException;
import com.example.models.SenderTarget;
import com.example.models.User;

import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.response.NotFoundException;

import javax.inject.Named;
import java.util.List;

@Api(
    name = "sample",
    version = "v1",
    scopes = {Constants.EMAIL_SCOPE},
    clientIds = {Constants.WEB_CLIENT_ID, Constants.ANDROID_CLIENT_ID, Constants.IOS_CLIENT_ID,
                 Constants.API_EXPLORER_CLIENT_ID},
    audiences = {Constants.ANDROID_AUDIENCE}
)
public class UserServiceImpl implements UserService {

    private UserDAO dao = new UserDAOJdo();

    @Override
    public User getUser(@Named("id") Long id) throws NotFoundException {
        User u = dao.getUser(id);
        if (u == null)
            throw new NotFoundException("User doesn't exist");
        return u;
    }

    @ApiMethod(
            name = "user.insert",
            path = "",
            httpMethod = ApiMethod.HttpMethod.POST
    )
    @Override
    public User insertUser(User u) {
        return dao.addUser(u);
    }

    @ApiMethod(
            name = "gcm_sender",
            path = "gcm_sender",
            httpMethod = ApiMethod.HttpMethod.POST
    )
    @Override
    public void gcmLauncher(SenderTarget target) throws GcmAlreadySengingException {
        if (GcmWorkerServlet.addTask(target.getRegId())) {

            Queue queue = QueueFactory.getDefaultQueue();
            TaskOptions options = TaskOptions.Builder.withUrl("/gcm_worker");
            options.param("target", target.getRegId());
            queue.add(options);

        } else {
            throw new GcmAlreadySengingException();
        }
    }

    @Override
    public List<User> listUser() {
        return dao.listUsers();
    }
}
