package com.example.services;

import com.example.exceptions.GcmAlreadySengingException;
import com.example.models.SenderTarget;
import com.example.models.ShortUser;
import com.example.models.User;
import com.google.api.server.spi.response.NotFoundException;

import java.util.List;

public interface UserService {

    public User getUser(Long id) throws NotFoundException;

    public List<User> listUser();

    public User insertUser(User u);

    public void gcmLauncher(SenderTarget t) throws GcmAlreadySengingException;
}
