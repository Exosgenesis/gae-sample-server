package com.example.services;

import com.example.Constants;
import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class GcmWorkerServlet extends HttpServlet {

    private final static List<String> activeDevices = new LinkedList<>();

    public static boolean addTask(String regId) {
        synchronized (activeDevices) {
            if (activeDevices.contains(regId)) {
                return false;
            } else {
                activeDevices.add(regId);
                return true;
            }
        }
    }

    private static void removeTask(String regId) {
        synchronized (activeDevices) {
            activeDevices.remove(regId);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        String target = req.getParameter("target");

        try {
            Thread.sleep(10000);
            for (int i = 2; i >= 0; i--) {
                sendMessageToDevice(target);
                if (i > 0)
                    Thread.sleep(5000);
            }

        } catch (InterruptedException ignored) {
        } finally {
            removeTask(target);
        }

        resp.setStatus(HttpServletResponse.SC_OK);
    }

    private Result sendMessageToDevice(String regId) {
        Sender sender = new Sender(Constants.ANDROID_API_KEY);
        Message gcmMessage =
                new Message.Builder()
                        .addData("title", "Hello from GAE! =)")
                        .addData("timestamp", String.valueOf(System.currentTimeMillis())).build();

        Result result = null;
        try {
            result = sender.send(gcmMessage, regId, 5);
            System.out.println("RegId: " + regId);
            System.out.println("Result on sent GCM: " + result.toString());

        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

}
