package com.example.models;

import java.io.Serializable;

public class SenderTarget implements Serializable {
    private String regId = "None";

    public SenderTarget() {
    }

    public String getRegId() {
        return regId;
    }

    public void setRegId(String regId) {
        this.regId = regId;
    }
}
